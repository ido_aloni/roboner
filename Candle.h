#pragma once


#define UV 36
#define FAN 37
#define candleInRoom() digitalRead(UV)?false:true
#define fanON() digitalWrite(FAN,HIGH)
#define fanOFF() digitalWrite(FAN,LOW)

double pyro1() {
	return analogRead(59U);
}

double pyro2() {
	return analogRead(57U);
}

double getPyroSum();

void red_led(bool state);
bool checkUV();

void p_straightCandleRight(double opt);
void p_straightCandleLeft(double opt);

void straightCandleRight(double opt);
void straightCandleLeft(double opt);

void straightCandle();

void blowAfterStraight();