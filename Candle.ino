
#include "Candle.h"


void red_led(bool state)
{
	digitalWrite(50, state?HIGH:LOW);
}

bool checkUV()
{
	volatile auto c = true;
	for (int i = 0; i < 100; i++) c &= !digitalRead(UV);
	return c;
}

void straightCandle()
{
	straightCandleRight(2);

	for (int i = 0; i < 2; i++) {
		p_straightCandleRight(2);
		p_straightCandleLeft(2);
	}
}

void straightCandleRight(double opt)
{
	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	double volatile cur = getPyroSum();
	auto volatile flag = true;
	while (flag)
	{
		auto volatile c = true;
		for (int i = 0; i < opt; i++)
		{
			c &= getPyroSum() < cur;
		}
		flag = !c;
		
		cur = 0;
		for (int i = 0; i < 5; i++)
			cur += getPyroSum();
		cur /= 5.0;
	}
	stop();
}

void straightCandleLeft(double opt)
{
	analogWrite(Motors::BACK_LEFT, 70);
	analogWrite(Motors::UP_RIGHT, 70);
	double volatile cur = getPyroSum();
	while (abs(getPyroSum() - cur) >= opt)
	{
		cur = getPyroSum();
	}
	stop();
}

void p_straightCandleRight(double opt)
{
	for (int i = 0; i < opt; i++) {
		analogWrite(Motors::UP_LEFT, 70);
		analogWrite(Motors::BACK_RIGHT, 70);
		double volatile cur = getPyroSum();
		while (getPyroSum() <= cur)
		{
			cur = getPyroSum();
		}
		stop();
	}
}

void p_straightCandleLeft(double opt)
{
	for (int i = 0; i < opt; i++) {
		analogWrite(Motors::BACK_LEFT, 70);
		analogWrite(Motors::UP_RIGHT, 70);
		double volatile cur = getPyroSum();
		while (getPyroSum() - cur <= opt)
		{
			cur = getPyroSum();
		}
		stop();
	}
}

void blowAfterStraight()
{
	stop();

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::UP_RIGHT, 100);
	
	
	while ((getDistance(Sensor::S_IR_F_LEFT) > 20 && getDistance(Sensor::S_IR_F_RIGHT) > 20) && (((pyro1() > 100 && pyro2() > 100)))) {


	}
	stop();

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::UP_RIGHT, 100);
	digitalWrite(37, HIGH);

	while ((getDistance(Sensor::S_IR_F_LEFT) > 20 && getDistance(Sensor::S_IR_F_RIGHT) > 20) && (!digitalRead(UV) || !digitalRead(UV)));

	stop();
	digitalWrite(37, LOW);

	if (!digitalRead(UV) || !digitalRead(UV))
	{
		digitalWrite(37, HIGH);
		analogWrite(Motors::UP_RIGHT, 70);
		analogWrite(Motors::BACK_LEFT, 70);
		while (!digitalRead(UV) || !digitalRead(UV));
		stop();
		digitalWrite(37, LOW);
	}

	/*
	bool flag = true;
	
	while ((!digitalRead(UV) && !digitalRead(UV) && !digitalRead(UV))) {

		if (flag) {
			analogWrite(Motors::BACK_LEFT, 100);
			analogWrite(Motors::UP_RIGHT, 0);
			delay(1000);
			flag = false;
		}
		else
		{
			analogWrite(Motors::UP_LEFT, 0);
			analogWrite(Motors::BACK_RIGHT, 100);
			delay(1000);
			flag = true;
		}
	}
	stop();*/
	
}

double getPyroSum()
{
	double volatile sum = 0;
	for (int i = 0; i < 100; i++)
		sum += (pyro1() + pyro2());
	sum /= 100.0;
	return sum;
}
