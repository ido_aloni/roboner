#pragma once
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_HMC5883_U.h>


#define addr 0x1E //I2C Address for The HMC5883

Adafruit_HMC5883_Unified gyro = Adafruit_HMC5883_Unified(12345);

typedef enum {X,Y,Z} Direction;

typedef struct GyroValues
{
	float x;
	float y;
	float z;

	GyroValues(float x, float y, float z) : x(x), y(y), z(z) {}

}GyroValues;

void initGyro();
GyroValues get_gyro_values();
float get_gyro_deg(Direction d1, Direction d2);
