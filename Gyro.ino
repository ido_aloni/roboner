#include "Gyro.h"


void initGyro()
{
	gyro.begin();
}

float get_gyro_deg(Direction d1, Direction d2)
{
	sensors_event_t event;
	gyro.getEvent(&event);

	// Hold the module so that Z is pointing 'up' and you can measure the heading with x&y
	// Calculate heading when the magnetometer is level, then correct for signs of axis.
	float heading = atan2((d1!=X)?(d1==Y? event.magnetic.y: event.magnetic.z):event.magnetic.x, (d2 != X) ? (d2 == Y ? event.magnetic.y : event.magnetic.z) : event.magnetic.x);

	// Once you have your heading, you must then add your 'Declination Angle', which is the 'Error' of the magnetic field in your location.
	// Find yours here: http://www.magnetic-declination.com/
	// Mine is: -13* 2' W, which is ~13 Degrees, or (which we need) 0.22 radians
	// If you cannot find your Declination, comment out these two lines, your compass will be slightly off.
	float declinationAngle = 0.08;
	heading += declinationAngle;

	// Correct for when signs are reversed.
	if (heading < 0)
		heading += 2 * PI;

	// Check for wrap due to addition of declination.
	if (heading > 2 * PI)
		heading -= 2 * PI;

	// Convert radians to degrees for readability.
	float headingDegrees = heading * 180.0 / M_PI;
	return headingDegrees;
}

GyroValues get_gyro_values()
{
	sensors_event_t event;
	gyro.getEvent(&event);
	return GyroValues(event.magnetic.x, event.magnetic.y, event.magnetic.z);
}