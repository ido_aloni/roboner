
#ifndef IDO_PING
#define IDO_PING

#define TEMP 24 //the temperature in the arena
#define D_SENSORS 12

#define IR_RIGHT A11
#define IR_LEFT A12
#define IR_F_LEFT A10
#define IR_F_RIGHT A9
#define IR_BACK A15

typedef enum { S_IR_RIGHT, S_IR_LEFT, S_IR_F_RIGHT, S_IR_F_LEFT, S_IR_BACK, S_LEFT_UP, S_LEFT_DOWN, S_RIGHT_UP, S_RIGHT_DOWN, KADIMA, BACK } Sensor;
typedef enum { LEFT, RIGHT } Side;


double getDistance(Sensor sensor);
double getDegrees(Side side);

void initSensors();

double SPEED_OF_SOUND;

#endif


