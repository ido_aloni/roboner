#include "iping.h"
#include <math.h>
#include <NewPing.h>

NewPing right_down(40, 41, 200);
NewPing right_up(42, 43, 200);
NewPing kadima(48, 49, 200);
NewPing back(50, 51,200);
NewPing left_up(46, 47, 200);
NewPing left_down(44, 45, 200);


void initSensors() {
	pinMode(IR_BACK, INPUT);
}


double getDegrees(Side side)
{
	double degRadians;

	if (side == Side::RIGHT) {

		double h1 = getDistance(Sensor::S_RIGHT_DOWN);
		double h2 = getDistance(Sensor::S_RIGHT_UP);
		return atan((h1 - h2) / D_SENSORS) * (180.0 / PI);

	}

	else if (side == Side::LEFT) {

		double h1 = getDistance(Sensor::S_LEFT_DOWN);
		double h2 = getDistance(Sensor::S_LEFT_UP);
		return atan((h1 - h2) / D_SENSORS) * (180.0 / PI);

	}

	return 0;
}




double getDistance(Sensor sensor) {

	if (sensor == Sensor::S_LEFT_UP) {

		return left_up.ping_cm();

	}
	else if (sensor == Sensor::S_LEFT_DOWN) {

		return left_down.ping_cm();

	}
	else if (sensor == Sensor::S_RIGHT_UP) {

		return right_up.ping_cm();

	}
	else if (sensor == Sensor::S_RIGHT_DOWN) {

		return right_down.ping_cm();

	}
	else if (sensor == Sensor::KADIMA) {

		return kadima.ping_cm();

	}
	else if (sensor == Sensor::BACK) {

		return back.ping_cm();
	}
	else if (sensor == Sensor::S_IR_RIGHT)
	{
		return abs(4800.0 / (analogRead(IR_RIGHT) - 20));
	}
	else if (sensor == Sensor::S_IR_LEFT)
	{
		return abs(4800.0 / (analogRead(IR_LEFT) - 20));
	}
	else if (sensor == Sensor::S_IR_F_LEFT)
	{
		return abs(4800.0 / (analogRead(IR_F_LEFT) - 20));
	}
	else if (sensor == Sensor::S_IR_F_RIGHT)
	{
		return abs(4800.0 / (analogRead(IR_F_RIGHT) - 20));
	}
	else if (sensor == Sensor::S_IR_BACK)
	{
		return abs(4800.0 / (analogRead(IR_BACK) - 20));
	}
}
