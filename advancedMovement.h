#pragma once

#include "basicMovement.h"

void begin();
void do_room_1();
void do_room_2();
void do_room_3();
void do_room_4();

void home_room_1(long t1, long t2);
void home_room_2(long t1, long t2);
void home_begin();

bool __start();