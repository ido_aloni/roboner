#include "advancedMovement.h"


void do_room_1()
{
	led(false);
	stop();

	// first half of turn
	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 120);
	while (abs(getDistance(Sensor::S_IR_RIGHT)) < 100); //80
	stop(); 

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (abs(getDistance(Sensor::S_IR_F_RIGHT)) < 50);
	stop(); 

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (abs(getDistance(Sensor::S_IR_F_LEFT)) < 80 );
	stop(); 

	analogWrite(Motors::UP_RIGHT, 120);
	analogWrite(Motors::UP_LEFT, 120);
	while (getDistance(Sensor::S_LEFT_DOWN) > 30 || getDistance(Sensor::S_RIGHT_DOWN) > 30);
	stop();


	if (getDistance(Sensor::S_LEFT_DOWN) > 30 && getDistance(Sensor::S_RIGHT_DOWN) > 30)
	{
		straightWallLeft();
	}
	else
	{
		analogWrite(Motors::UP_RIGHT, 120);
		analogWrite(Motors::UP_LEFT, 120);
		while (getDistance(Sensor::S_LEFT_DOWN) > 30 || getDistance(Sensor::S_RIGHT_DOWN) > 30);
		stop();
	}


	if (getDistance(Sensor::S_LEFT_DOWN) > 30 && getDistance(Sensor::S_RIGHT_DOWN) > 30)
	{
		straightWallLeft();
	}
	else
	{
		analogWrite(Motors::UP_RIGHT, 120);
		analogWrite(Motors::UP_LEFT, 120);
		while (getDistance(Sensor::S_LEFT_DOWN) > 30 || getDistance(Sensor::S_RIGHT_DOWN) > 30);
		stop();
	}

	stop();
	delay(50);
	straightWallLeft();
	while (!_P_followLeftWall(12) && getDistance(Sensor::KADIMA) > 30); //30
	stop(); 
	delay(50);
	if (getDistance(Sensor::KADIMA) > 22)
	{
		while (!_P_followLeftWall(12) && getDistance(Sensor::KADIMA) > 30); //30
		stop();
	}
	// turnRight();
	// while (getDistance(Sensor::S_IR_RIGHT) < 80);  
	straightWallLeft();
	analogWrite(Motors::BACK_RIGHT, 100);
	analogWrite(Motors::UP_LEFT, 100);
	while (getDistance(Sensor::S_IR_F_LEFT) <70);
	stop();
	delay(50);
	straightWallLeft();
	//todo: need to fix the pniia - this not always succeed sometimes keeps on no finish turn
	// move a bit forward - cross white line
	analogWrite(Motors::UP_RIGHT, BASIC_V);
	analogWrite(Motors::UP_LEFT, BASIC_V);
	// while (getDistance(Sensor::S_RIGHT_DOWN) > 30 || getDistance(Sensor::BACK) < 15);
	while (getDistance(Sensor::S_RIGHT_DOWN) > 50 || getDistance(Sensor::S_RIGHT_DOWN) > 50);
	stop();
	straightWallLeft();
	analogWrite(Motors::UP_RIGHT, BASIC_V);
	analogWrite(Motors::UP_LEFT, BASIC_V);
	// while (getDistance(Sensor::S_RIGHT_DOWN) > 30 || getDistance(Sensor::BACK) < 15);
	while (getDistance(Sensor::S_RIGHT_DOWN) > 50 || getDistance(Sensor::S_RIGHT_DOWN) > 50);
	stop();
	straightWallLeft();
	delay(10);

	// blow ittttt
	if (!digitalRead(UV) || !digitalRead(UV) || !digitalRead(UV))
	{
		red_led(true);
		long t2 = millis();
		straightCandle();
		stop();
		t2 = millis() - t2;
		delay(10);
		long t = millis();
		blowAfterStraight(); stop();
		t = millis() - t;
		red_led(false);
		home_room_1(t, t2);
		while (1);
	}

	straightWallLeft();
	delay(10);
	// go back
	analogWrite(Motors::BACK_RIGHT, 140);
	analogWrite(Motors::BACK_LEFT, 140);
	while (getDistance(Sensor::S_RIGHT_UP) < 80);
	while (getDistance(Sensor::BACK) > 15 || getDistance(Sensor::BACK) > 15);
	stop(); 

	straightWallLeft();
	delay(10);
	// go back
	analogWrite(Motors::BACK_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::BACK) > 15);
	stop();

	// turn
	analogWrite(Motors::BACK_RIGHT, 120);
	analogWrite(Motors::UP_LEFT, 120);
	while (abs(getDistance(Sensor::S_IR_LEFT)) < 60);
	stop(); 

	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 120);
	while (abs(getDistance(Sensor::S_IR_RIGHT)) < 80);
	stop();

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (abs(getDistance(Sensor::S_IR_F_RIGHT)) < 50);
	stop();

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (abs(getDistance(Sensor::S_IR_F_LEFT)) < 50);
	stop(); delay(50);

	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 120);
	while (abs(getDistance(Sensor::S_IR_RIGHT)) > 30);
	stop();

	// get out of the room
	straightWallRight(); delay(50);

	/*
	double d = (getDistance(Sensor::S_RIGHT_UP) + getDistance(Sensor::S_RIGHT_DOWN)) / 2.0;
	while (!_P_followRightWall(d) && getDistance(Sensor::S_LEFT_DOWN) > 30); // prev distance was const 15
	stop();

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (getDistance(Sensor::S_RIGHT_UP) > getDistance(Sensor::S_RIGHT_DOWN));
	stop();

	analogWrite(Motors::UP_LEFT, BASIC_V);
	analogWrite(Motors::UP_RIGHT, BASIC_V);
	while (getDistance(Sensor::S_RIGHT_DOWN) < 50);
	stop();
	*/

	auto d = (getDistance(Sensor::S_RIGHT_DOWN) + getDistance(Sensor::S_RIGHT_UP)) / 2.0;
	while (!_P_m_followRightWall(d > 10 ? 10 : 7, 150) && getDistance(Sensor::S_RIGHT_UP) < 30); // prev distance was const 15
	stop(); 

	 d = (getDistance(Sensor::S_RIGHT_DOWN) + getDistance(Sensor::S_RIGHT_UP)) / 2.0;
	while (!_P_m_followRightWall(d > 10 ? 10 : 7, 150) && getDistance(Sensor::S_RIGHT_UP) < 30); // prev distance was const 15
	stop();

	/*
	analogWrite(UP_RIGHT, 100);
	analogWrite(UP_LEFT, 100);
	while (getDistance(Sensor::S_RIGHT_DOWN) < 30);
	stop(); 
	*/

	analogWrite(Motors::UP_LEFT, 140);
	analogWrite(Motors::BACK_RIGHT, 0);
	while (getDistance(Sensor::BACK) >30);
	stop(); delay(50);

	analogWrite(Motors::UP_LEFT, 140);
	analogWrite(Motors::BACK_RIGHT, 0);
	while (getDistance(Sensor::BACK) >30);
	stop(); delay(50);


	analogWrite(Motors::UP_LEFT, 140);
	analogWrite(Motors::BACK_RIGHT, 0);
	while (getDistance(Sensor::BACK) <100);
	stop(); delay(50);


	analogWrite(Motors::UP_LEFT, 140);
	analogWrite(Motors::BACK_RIGHT, 0);
	while (getDistance(Sensor::BACK) <100);
	stop(); delay(50);


	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 0);
	while (abs(getDistance(Sensor::S_IR_RIGHT)) >32 || abs(getDistance(Sensor::S_IR_F_LEFT) < 100));
	stop(); delay(10);

	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 0);
	while (abs(getDistance(Sensor::S_IR_RIGHT)) >34);
	stop(); delay(10);


	analogWrite(Motors::UP_LEFT, 140);
	analogWrite(Motors::UP_RIGHT, 140);
	while (getDistance(Sensor::S_RIGHT_DOWN) > 40 || getDistance(Sensor::S_RIGHT_DOWN) > 40);
	stop();
	delay(10);
	analogWrite(Motors::UP_LEFT, 140);
	analogWrite(Motors::UP_RIGHT, 140);
	while (getDistance(Sensor::S_RIGHT_DOWN) > 40 || getDistance(Sensor::S_RIGHT_DOWN) > 40);
	stop(); delay(10);

	auto volatile c = true;
	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::UP_RIGHT, 100);
	do {
		
		c = true;

		for (int i = 0; i < 25; i++)
			c = c && getDistance(Sensor::S_RIGHT_DOWN)>40;

	} while (c);
	stop();

	if (getDistance(Sensor::S_RIGHT_DOWN) > 30 && getDistance(Sensor::S_RIGHT_DOWN) > 30)
		if (getDistance(Sensor::S_RIGHT_DOWN) > 30 && getDistance(Sensor::S_RIGHT_DOWN) > 30)
			straightWallRight(); 
	
	stop(); 

}

void home_room_1(long t1, long t2)
{
	analogWrite(Motors::BACK_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	long t = millis();
	while (millis() - t < t1 && getDistance(Sensor::S_IR_BACK) > 18);
	stop();
	analogWrite(Motors::UP_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	long l = millis();
	while (millis() - l < t2 && getDistance(Sensor::S_IR_LEFT) > 30);
	stop();
	straightWallLeft();
	straightWallLeft();
	delay(10);
	// go back
	analogWrite(Motors::BACK_RIGHT, 140);
	analogWrite(Motors::BACK_LEFT, 140);
	while (getDistance(Sensor::BACK) > 13 || getDistance(Sensor::BACK) > 13);
	stop();

	// turn
	analogWrite(Motors::BACK_RIGHT, 120);
	analogWrite(Motors::UP_LEFT, 120);
	while (abs(getDistance(Sensor::S_IR_LEFT)) < 60);
	stop();

	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 120);
	while (abs(getDistance(Sensor::S_IR_RIGHT)) < 80);
	stop();

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (abs(getDistance(Sensor::S_IR_F_RIGHT)) < 50);
	stop();

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (abs(getDistance(Sensor::S_IR_F_LEFT)) < 50);
	stop(); delay(50);

	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 120);
	while (abs(getDistance(Sensor::S_IR_RIGHT)) > 30);
	stop();

	// get out of the room
	straightWallRight(); delay(50);

	led(true);

	auto d = (getDistance(Sensor::S_RIGHT_DOWN) + getDistance(Sensor::S_RIGHT_UP)) / 2.0;
	while (!_P_m_followRightWall(d > 10 ? 10 : 7, 100) && getDistance(Sensor::S_RIGHT_UP) < 30); // prev distance was const 15
	stop(); 

	 d = (getDistance(Sensor::S_RIGHT_DOWN) + getDistance(Sensor::S_RIGHT_UP)) / 2.0;
	 while (!_P_m_followRightWall(d > 10 ? 10 : 7, 100) && getDistance(Sensor::S_RIGHT_UP) < 30); // prev distance was const 15
	 stop();


	analogWrite(UP_RIGHT, 100);
	analogWrite(UP_LEFT, 100);
	while (getDistance(Sensor::S_RIGHT_DOWN) < 20 || getDistance(Sensor::S_LEFT_DOWN) < 20);
	stop();

	analogWrite(Motors::UP_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::S_IR_F_LEFT) > 40);
	stop();

	analogWrite(Motors::UP_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::S_IR_F_LEFT) > 40);
	stop();


	analogWrite(Motors::UP_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::S_IR_F_LEFT) < 100);
	stop();


	analogWrite(Motors::UP_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::S_IR_F_LEFT) < 100);
	stop(); 

	analogWrite(Motors::UP_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::S_IR_LEFT) > 34);
	stop();

	analogWrite(Motors::UP_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::S_IR_LEFT) > 34);
	stop();

	analogWrite(Motors::UP_LEFT, 140);
	analogWrite(Motors::UP_RIGHT, 140);
	while (getDistance(Sensor::S_LEFT_DOWN) > 30);
	stop();


	analogWrite(Motors::UP_LEFT, 140);
	analogWrite(Motors::UP_RIGHT, 140);
	while (getDistance(Sensor::S_LEFT_DOWN) > 30);
	stop();


	analogWrite(Motors::UP_LEFT, 140);
	analogWrite(Motors::UP_RIGHT, 140);
	while (getDistance(Sensor::S_LEFT_DOWN) > 30);
	stop();

	straightWallLeft();

	while (!_P_m_followLeftWall(10, 100) && getDistance(Sensor::KADIMA) > 20);
	stop();
}

void do_room_2()
{
	stop(); 

	// get a little closer
	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::UP_RIGHT, 120);
	while (getDistance(Sensor::KADIMA) > 22);
	stop(); 

	// turn into the room
	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 120);
	while (getDistance(Sensor::S_IR_RIGHT) < 80);
	stop();

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (getDistance(Sensor::S_IR_F_RIGHT) < 50);
	stop();

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (getDistance(Sensor::S_IR_F_LEFT) < 50);
	stop();

	straightWallLeft();

	beep();
	
	// cross the white line
	analogWrite(Motors::UP_RIGHT, 120);
	analogWrite(Motors::UP_LEFT, 120);
	while (getDistance(Sensor::S_RIGHT_DOWN) > 50 || getDistance(Sensor::KADIMA) > 60);
	stop(); 
	
	// check and blow
	// blow ittttt
	if (checkUV())
	{
		red_led(true);
		long t1 = millis();
		straightCandle();
		t1 = millis() - t1;
		long t2 = millis();
		blowAfterStraight(); stop();
		t2 = millis() - t2;
		red_led(false);
		home_room_2(t2, t1);
		while (1);
	}

	// turn 180
	// first turn
	analogWrite(Motors::BACK_RIGHT, 120);
	analogWrite(Motors::UP_LEFT, 120);
	while (getDistance(Sensor::S_IR_RIGHT) > 30);
	stop(); 

	// here check candle
	// blow ittttt
	if (checkUV())
	{
		red_led(true);
		straightCandleRight(20);
		stop(); delay(10);
		straightCandleLeft(20);
		stop(); delay(10);
		blowAfterStraight(); stop();
		red_led(false);
		while (1);
	}

	beep(); 

	// end turn
	analogWrite(Motors::BACK_RIGHT, 120);
	analogWrite(Motors::UP_LEFT, 120);
	while (getDistance(Sensor::S_IR_LEFT) > 30);
	stop(); 

	straightWallRight();

	// cross whiteline
	while (!_P_followRightWall(20) && getDistance(Sensor::S_LEFT_DOWN) > 30);
	stop();
	straightWallRight();
}

void home_room_2(long t1, long t2)
{
	analogWrite(Motors::BACK_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	long l = millis();
	while (millis() - l < t1);
	stop();
	analogWrite(Motors::BACK_LEFT, 100);
	analogWrite(Motors::UP_RIGHT, 100);
	l = millis();
	while (millis() - l < t2 && getDistance(Sensor::S_IR_LEFT) > 30);
	stop();
	straightWallLeft();
	analogWrite(Motors::BACK_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (getDistance(Sensor::KADIMA) < 80);
	stop();
	analogWrite(Motors::BACK_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (getDistance(Sensor::KADIMA) < 80);
	stop();
	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (getDistance(Sensor::S_IR_F_LEFT) > 30);
	stop();
	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (getDistance(Sensor::S_IR_F_LEFT) < 100);
	stop();
	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::UP_LEFT, 100);
	while (getDistance(Sensor::S_LEFT_DOWN) > 40);
	stop();
	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::UP_LEFT, 100);
	while (getDistance(Sensor::S_LEFT_DOWN) > 40);
	stop();
	straightWallLeft();
	stop();
}

void do_room_3()
{
	straightWallRight();

	// there is wall
	if (getDistance(Sensor::KADIMA) < 80 && getDistance(Sensor::KADIMA) < 80)
	{

		// get to the room
		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::UP_LEFT, 140);
		while (getDistance(Sensor::KADIMA) > 20); //20
		stop(); delay(20); straightWallRight();

		stop(); delay(20);
		straightWallRight();

		// turn 
		//	turnLeft();
		analogWrite(Motors::UP_RIGHT, 0);
		analogWrite(Motors::BACK_LEFT, 200);
		while (getDistance(Sensor::S_IR_LEFT) < 50);
		stop();
		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::S_IR_F_RIGHT) < 50);
		stop(); delay(50);

		
		straightWallRight();

		// move forward to the wall
		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::UP_LEFT, 140);
		while ((getDistance(Sensor::S_RIGHT_DOWN) < 40 || getDistance(Sensor::S_LEFT_DOWN) > 40));
		stop();

		analogWrite(Motors::UP_RIGHT, 120);
		analogWrite(Motors::UP_LEFT, 120);
		while ((getDistance(Sensor::S_RIGHT_DOWN) < 40 || getDistance(Sensor::S_LEFT_DOWN) > 40));
		stop();

		analogWrite(Motors::UP_RIGHT, 100);
		analogWrite(Motors::UP_LEFT, 100);
		while ((getDistance(Sensor::S_RIGHT_DOWN) < 40 || getDistance(Sensor::S_LEFT_DOWN) > 40));
		stop();

		// turn inside
		/*
		analogWrite(Motors::BACK_RIGHT, 0);
		analogWrite(Motors::UP_LEFT, 140);
		while (getDistance(Sensor::S_IR_LEFT) < 60); stop();
		analogWrite(Motors::BACK_RIGHT, 0);
		analogWrite(Motors::UP_LEFT, 140);
		while (getDistance(Sensor::S_IR_LEFT) > 20); stop();
		*/
		/*
		analogWrite(Motors::BACK_RIGHT, 0);
		analogWrite(Motors::UP_LEFT, 140);
		while (getDistance(Sensor::S_IR_F_LEFT) > 30); stop();
		analogWrite(Motors::BACK_RIGHT, 0);
		analogWrite(Motors::UP_LEFT, 140);
		while (getDistance(Sensor::S_IR_F_LEFT) < 43); stop();
		*/

		// turn in

		analogWrite(Motors::BACK_RIGHT, 0);
		analogWrite(Motors::UP_LEFT, 120);
		while (getDistance(Sensor::BACK) > 18 && getDistance(Sensor::S_IR_F_LEFT) > 30); stop();
		stop();
		delay(10);

		analogWrite(Motors::BACK_RIGHT, 0);
		analogWrite(Motors::UP_LEFT, 120);
		while (getDistance(Sensor::S_IR_LEFT) < 50); stop();
		stop();
		delay(10);

		analogWrite(Motors::BACK_RIGHT, 0);
		analogWrite(Motors::UP_LEFT, 120);
		while (getDistance(Sensor::S_IR_BACK) > 25); stop();
		stop();
		delay(10);

		analogWrite(Motors::BACK_RIGHT, 0);
		analogWrite(Motors::UP_LEFT, 120);
		while (getDistance(Sensor::S_IR_F_LEFT) > 40); stop();
		stop();

		delay(1000);

		analogWrite(Motors::BACK_RIGHT, 0);
		analogWrite(Motors::UP_LEFT, 120);
		while (getDistance(Sensor::S_IR_F_LEFT) < 100 && getDistance(Sensor::S_IR_F_RIGHT) < 60); stop();
		stop();

		analogWrite(Motors::BACK_RIGHT, 0);
		analogWrite(Motors::UP_LEFT, 120);
		while (getDistance(Sensor::S_IR_F_LEFT) < 100); stop();
		stop();
		delay(10);


		// get inside
		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::UP_LEFT, 140);
		while (getDistance(Sensor::S_LEFT_DOWN) > 30 || getDistance(Sensor::BACK) < 30);
		stop();

		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::UP_LEFT, 140);
		while (getDistance(Sensor::S_LEFT_DOWN) > 30 || getDistance(Sensor::BACK) < 30);
		stop();
		
		straightWallLeft();

		// first candle check
		// blow ittttt
		if (checkUV())
		{
			red_led(true);
			straightCandle();
			stop(); delay(10);
			blowAfterStraight(); stop();
			red_led(false);
			while (1);
		}

		// turn
		stop(); delay(50);
		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::UP_LEFT, 100);
		while (abs(getDistance(Sensor::S_IR_F_RIGHT)) < 40);
		stop();
		delay(10);
		analogWrite(Motors::BACK_RIGHT, 0);
		analogWrite(Motors::UP_LEFT, 100);
		while (abs(getDistance(Sensor::S_IR_RIGHT)) > 34);
		stop();


		// second candle check
		// blow ittttt
		if (checkUV())
		{
			red_led(true);
			straightCandle();
			stop(); delay(10);
			blowAfterStraight(); stop();
			red_led(false);
			while (1);
		}

		// turn back

		delay(50);
		analogWrite(Motors::UP_RIGHT, 100);
		analogWrite(Motors::UP_LEFT, 100);
		while (getDistance(Sensor::BACK) < 18);
		stop(); delay(10);

		delay(50);
		analogWrite(Motors::UP_RIGHT, 0);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::S_IR_LEFT) > 34);
		stop(); delay(10);

		delay(50);
		analogWrite(Motors::UP_RIGHT, 0);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::S_IR_LEFT) > 34);
		stop(); delay(10);
	
		if (getDistance(Sensor::S_LEFT_UP) < 20 && getDistance(Sensor::S_LEFT_DOWN) < 20)
			straightWallLeft();


		if (getDistance(Sensor::S_LEFT_UP) < 20 && getDistance(Sensor::S_LEFT_DOWN) < 20)
			straightWallLeft();

		// drive back
		analogWrite(Motors::BACK_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 140);
		while (getDistance(Sensor::S_LEFT_UP) < 30);
		stop();

		analogWrite(Motors::BACK_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 140);
		while (getDistance(Sensor::S_LEFT_UP) < 30);
		stop();

		delay(50);
		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::BACK) > 20);
		stop(); delay(10);

		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 140);
		while (getDistance(Sensor::S_IR_LEFT) < 60);
		stop();

		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 140);
		while (getDistance(Sensor::S_IR_LEFT) > 34);
		stop(); 

		if (getDistance(Sensor::S_LEFT_DOWN) <30 && getDistance(Sensor::S_LEFT_UP) <30)
			straightWallLeft();


		if (getDistance(Sensor::S_LEFT_DOWN) <30 && getDistance(Sensor::S_LEFT_UP) <30)
			straightWallLeft();


		delay(50);
		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::UP_LEFT, 140);
		while (getDistance(Sensor::S_LEFT_UP) < 40);
		stop();

		
		delay(50);
		analogWrite(Motors::BACK_RIGHT, 50);
		analogWrite(Motors::BACK_LEFT, 50);
		while (getDistance(Sensor::S_RIGHT_UP) > 20);
		stop();

		delay(50);
		analogWrite(Motors::BACK_RIGHT, 50);
		analogWrite(Motors::BACK_LEFT, 50);
		while (getDistance(Sensor::S_RIGHT_UP) > 20);
		stop();


	}
	// no wall
	else
	{
		// enter the room
		/*
		analogWrite(Motors::UP_LEFT, 140);
		analogWrite(Motors::UP_RIGHT, 140);
		while (getDistance(Sensor::BACK) <88);
		stop();*/ 


		while (!_P_m_followRightWall(20, 150) && (getDistance(Sensor::BACK) < 80)); // 15 , 100
		stop(); delay(10); straightWallRight(); delay(50); straightWallRight();

		analogWrite(Motors::UP_LEFT, 70); //70
		analogWrite(Motors::UP_RIGHT, 70);
		volatile double value = 0;
		volatile auto c = false;
		do
		{
			for (int i = 0, value = 0; i < 20; i++)
				value += getDistance(Sensor::S_LEFT_DOWN);
			value /= 20.0;
			c |= getDistance(Sensor::S_LEFT_DOWN) < 30;
		} while (!c);

		stop();

		// fix

		analogWrite(Motors::UP_LEFT, 70);
		analogWrite(Motors::UP_RIGHT, 70);
		value = 0;
		c = false;
		do
		{
			/*
			for (int i = 0, c = false; i < 1; i++)
			c |= (getDistance(Sensor::S_LEFT_DOWN) != 0 && getDistance(Sensor::S_LEFT_DOWN) < 30 || ((getDistance(Sensor::S_LEFT_UP) > 100 || getDistance(Sensor::S_LEFT_DOWN) > 100)));
			*/
			for (int i = 0, value = 0; i < 10; i++)
				value += getDistance(Sensor::S_LEFT_DOWN);
			value /= 10.0;
			c |= getDistance(Sensor::S_LEFT_DOWN) < 30;
		} while (!c);


		//while (!_P_followRightWall(14) && (getDistance(Sensor::S_LEFT_UP) > 100 || getDistance(Sensor::S_LEFT_DOWN) > 100));
		stop(); straightWallRight();
		//analogWrite(Motors::UP_LEFT, 100); analogWrite(Motors::UP_RIGHT,100);
		//while (getDistance(Sensor::S_LEFT_DOWN) > 60); stop();
		//while (!_P_followRightWall(15) &&getDistance(Sensor::S_LEFT_DOWN) > 40);
		//while (!_P_followRightWall(15) && getDistance(Sensor::S_LEFT_DOWN) > 40);
		stop(); beep(); 

		// first candle check
		// blow ittttt
		if (checkUV())
		{
			red_led(true);
			straightCandle();
			stop(); delay(10);
			blowAfterStraight(); stop();
			red_led(false);
			while (1);
		}

		// turn
		delay(50);
		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 0);
		while (getDistance(Sensor::S_IR_LEFT) > 32);
		stop(); beep();

		analogWrite(Motors::UP_LEFT, 140);
		analogWrite(Motors::UP_RIGHT, 140);
		while (getDistance(Sensor::BACK) < 16);
		stop(); beep();

		// second candle check
		// blow ittttt
		  if (checkUV())
		{
			red_led(true);
			straightCandle();
			stop(); delay(10);
			blowAfterStraight(); stop();
			red_led(false);
			while (1);
		}

		// turn back

		delay(50);
		analogWrite(Motors::BACK_RIGHT, 140);
		analogWrite(Motors::UP_LEFT, 0);
		while (abs(getDistance(Sensor::S_IR_LEFT)) < 72);
		stop();
		analogWrite(Motors::BACK_RIGHT, 140);
		analogWrite(Motors::UP_LEFT, 0);
		while (abs(getDistance(Sensor::S_IR_RIGHT)) > 30);
		stop();
		straightWallRight();
		// fix

		// get back to the entrance of room 2
		analogWrite(Motors::BACK_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 140);
		while (getDistance(Sensor::S_LEFT_UP) <80 || getDistance(Sensor::S_LEFT_DOWN) < 80);
		stop(); 
		straightWallRight(); 
		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::S_LEFT_DOWN) > 30 || getDistance(Sensor::BACK) > 70);
		stop();
		
		// turn 90 to keep on

		/*
		stop();
		straightWallRight();
		delay(50);
		while (!_P_reverseRightWall(14) && (getDistance(Sensor::BACK) > 65 || getDistance(Sensor::S_LEFT_DOWN) < 100 || getDistance(Sensor::S_LEFT_UP) < 100));
		stop(); delay(1000);

		stop();
		straightWallRight();
		delay(50);
		analogWrite(Motors::BACK_LEFT, 100);
		analogWrite(Motors::BACK_RIGHT, 100);
		c = false;
		do
		{
			
			c |= getDistance(Sensor::S_LEFT_DOWN) < 40 && getDistance(Sensor::S_LEFT_DOWN) != 0;
		} while (!c);
		stop(); straightWallRight(); delay(1000);
		*/
		/*
		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 0);
		while (abs(getDistance(Sensor::S_IR_RIGHT)) < 300);
		stop(); beep();
		
		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 0);
		while (abs(getDistance(Sensor::S_IR_LEFT)) > 28);
		stop(); beep(); 
		*/


		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 0);
		while (abs(getDistance(Sensor::BACK)) > 22);
		stop(); delay(10);

		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 0);
		while (abs(getDistance(Sensor::S_IR_LEFT)) > 33);
		delay(50);
		// go up to the door of room 3
		analogWrite(Motors::UP_LEFT, 140);
		analogWrite(Motors::UP_RIGHT, 140);
		while (getDistance(Sensor::S_LEFT_DOWN) > 30 || getDistance(Sensor::BACK) < 30);
		stop(); 

		// go to the end of the door
		while (getDistance(Sensor::S_RIGHT_UP) < 40 && !_P_followRightWall(10));
		stop();
	}

	delay(100);

	// turn to room 4
	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 0);
	while (getDistance(Sensor::S_IR_F_LEFT) > 40);
	stop(); 
	
	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 0);
	while (getDistance(Sensor::S_IR_F_LEFT) > 40);
	stop();
	
	delay(10);
	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 0);
	while (getDistance(Sensor::S_IR_LEFT) < 80);
	stop();

	delay(10);
	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 0);
	while (getDistance(Sensor::S_IR_RIGHT) < 70);
	stop();

	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 0);
	while (getDistance(Sensor::S_IR_F_LEFT) < 120);
	stop();

	delay(10);
	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 120);
	while (getDistance(Sensor::S_IR_RIGHT) > 30);
	stop();
	delay(10);
	
	delay(10);
	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::BACK_RIGHT, 120);
	while (getDistance(Sensor::S_IR_RIGHT) > 30);
	stop();
	delay(10);

	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::UP_RIGHT, 120);
	while (getDistance(Sensor::S_RIGHT_DOWN) > 30 || getDistance(Sensor::S_RIGHT_DOWN) > 30 || getDistance(Sensor::S_RIGHT_DOWN) > 30);
	stop(); delay(10);

	analogWrite(Motors::UP_LEFT, 120);
	analogWrite(Motors::UP_RIGHT, 120);
	while (getDistance(Sensor::S_RIGHT_DOWN) > 30 || getDistance(Sensor::S_RIGHT_DOWN) > 30 || getDistance(Sensor::S_RIGHT_DOWN) > 30);
	stop(); delay(10); 

}

void do_room_4()
{
	straightWallRight();
	stop();

	volatile auto c = 0;

	for (int i = 0; i < 5; i++)
		c += getDistance(Sensor::S_LEFT_UP);

	c /= 5.0;

	if (c > 30)
	{
		led(true);
		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 0);
		while (getDistance(Sensor::BACK) > 16);
		stop(); 

		analogWrite(Motors::UP_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (abs(getDistance(Sensor::S_IR_LEFT)) > 34);
		stop(); delay(10);

		analogWrite(Motors::UP_RIGHT, 100);
		analogWrite(Motors::UP_LEFT, 100);
		while (abs(getDistance(Sensor::S_RIGHT_DOWN)) > 30);
		stop(); 
		straightWallLeft();
		
		// blow ittttt
		  if (checkUV())
		{
			red_led(true);
			straightCandle();
			stop(); delay(10);
			blowAfterStraight(); stop();
			red_led(false);
			while (1);
		}

		stop(); delay(50);
		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::S_LEFT_UP) < 30);
		stop();
		delay(50);

		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::BACK) > 12);
		stop();
		delay(50);

		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::BACK) > 12);
		stop();
		delay(50);

		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::UP_LEFT, 100);
		while (getDistance(Sensor::KADIMA) < 80); stop();

		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::UP_LEFT, 100);
		while (abs(getDistance(Sensor::S_IR_F_LEFT)) < 150); 
		stop(); 
		straightWallRight();
		led(false);
	}

	straightWallRight();
	analogWrite(Motors::UP_LEFT, BASIC_V);
	analogWrite(Motors::UP_RIGHT, BASIC_V);
	while (getDistance(Sensor::S_RIGHT_DOWN) < 30 || getDistance(Sensor::KADIMA) > 30);
	stop(); delay(50);

	analogWrite(Motors::UP_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (abs(getDistance(Sensor::S_IR_F_LEFT)) < 120); stop(); 
	straightWallRight();

	delay(50);
	while (!_P_followRightWall(14) && getDistance(Sensor::KADIMA) > 30);
	stop(); straightWallRight(); 
	delay(50);

	while (!_P_followRightWall(14) && getDistance(Sensor::KADIMA) > 30);
	stop(); straightWallRight();
	delay(50);
	turnLeft();

	if (abs(getDistance(Sensor::S_IR_F_LEFT) < 100))
	{
		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 140);
		while (abs(getDistance(Sensor::S_IR_F_LEFT) < 100)); stop();
	}
	stop(); straightWallRight();
	delay(10);

	while (!_P_followRightWall(14) && getDistance(Sensor::BACK) < 45);
	stop(); delay(10); straightWallRight(); delay(10);
	
	if (getDistance(Sensor::S_LEFT_UP) > 30|| getDistance(Sensor::S_LEFT_UP) > 30|| getDistance(Sensor::S_LEFT_UP) > 30)
	{
		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 140);
		while (getDistance(Sensor::BACK) > 20);
		stop(); delay(50);

		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 140);
		while (abs(getDistance(Sensor::S_IR_LEFT)) > 40);
		stop();

		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::UP_LEFT, 140);
		while (getDistance(Sensor::S_LEFT_DOWN) > 28 || getDistance(Sensor::S_RIGHT_DOWN) > 50); stop();

		if (getDistance(Sensor::S_LEFT_DOWN) < 28)
			straightWallLeft();

		// blow ittttt
		  if (checkUV())
		{
			red_led(true);
			straightCandle();
			stop(); delay(10);
			blowAfterStraight(); stop();
			red_led(false);
			while (1);
		}

		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::BACK) > 10 || getDistance(Sensor::S_LEFT_UP) < 30); stop();

		delay(50);

		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::BACK) > 15); stop();

		analogWrite(Motors::UP_RIGHT, 140);
		analogWrite(Motors::BACK_LEFT, 140);
		while (getDistance(Sensor::BACK) < 70); stop();

		analogWrite(Motors::UP_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::S_IR_LEFT) > 28); stop();
		straightWallLeft();

		while (!_P_reverseLeftWall(10) && getDistance(Sensor::BACK) > 20); stop();
		while (!_P_reverseLeftWall(10) && getDistance(Sensor::BACK) > 20); stop();
		while (!_P_reverseLeftWall(10) && getDistance(Sensor::BACK) > 20); stop();
	}
}

void begin()
{
	led(false);
	// face to dog

	if (!(getDistance(Sensor::S_RIGHT_UP) > 30 || getDistance(Sensor::S_RIGHT_DOWN) > 30))
	{
		analogWrite(Motors::BACK_LEFT, 100);
		analogWrite(Motors::BACK_RIGHT, 100);
		while (getDistance(Sensor::BACK) > 8);
		stop();
		straightWallRight();
		analogWrite(Motors::UP_RIGHT, 120);
		analogWrite(Motors::BACK_LEFT, 120);
		//while (getDistance(Sensor::S_LEFT_UP) > 20 && getDistance(Sensor::S_LEFT_DOWN) > 20);
		while (abs(getDistance(Sensor::S_IR_RIGHT)) < 60); stop(); 
		analogWrite(Motors::UP_RIGHT, 120);
		analogWrite(Motors::BACK_LEFT, 120);
		while (abs(getDistance(Sensor::S_IR_RIGHT)) > 30 && getDistance(Sensor::S_IR_LEFT) > 30); stop(); 
		//while (getDistance(Sensor::S_LEFT_UP) > 20 && getDistance(Sensor::S_LEFT_DOWN) > 20);
		analogWrite(Motors::UP_RIGHT, 120);
		analogWrite(Motors::BACK_LEFT, 120);
		while ((getDistance(Sensor::S_LEFT_UP) == 0 || getDistance(Sensor::S_LEFT_DOWN) == 0 )||( getDistance(Sensor::S_LEFT_UP) > 20 && getDistance(Sensor::S_LEFT_DOWN) > 20));
		stop(); 
		straightWallLeft();
	}
	
	{
		auto c = 0;
		straightWallLeft();
		for (int i = 0; i < 100; i++)
			if (abs(getDistance(Sensor::S_IR_F_LEFT)) < 60 || abs(getDistance(Sensor::S_IR_F_RIGHT)) < 40) c++;

		if (c > 50)
		{
			delay(10);
		}
		else
		{
			straightWallLeft(); /*
			analogWrite(Motors::UP_RIGHT, 140);
			analogWrite(Motors::UP_LEFT, 140); */
			auto d = (getDistance(Sensor::S_LEFT_DOWN) + getDistance(Sensor::S_LEFT_UP)) / 2.0;
			while (!_P_m_followLeftWall(d < 7 ? d + 2 : d, BASIC_V) && getDistance(Sensor::S_RIGHT_UP) > 30); stop(); straightWallLeft();

			 d = (getDistance(Sensor::S_LEFT_DOWN) + getDistance(Sensor::S_LEFT_UP)) / 2.0;
			while (!_P_m_followLeftWall(d < 7 ? d + 2 : d, 120) && getDistance(Sensor::S_RIGHT_DOWN) > 30 && getDistance(Sensor::S_IR_F_LEFT) >20 && getDistance(Sensor::S_IR_F_RIGHT) >20); stop(); straightWallLeft();

			d = (getDistance(Sensor::S_LEFT_DOWN) + getDistance(Sensor::S_LEFT_UP)) / 2.0;
			while (!_P_m_followLeftWall(d < 7 ? d + 2 : d, 120) && getDistance(Sensor::S_RIGHT_DOWN) > 30 && getDistance(Sensor::S_IR_F_LEFT) >20 && getDistance(Sensor::S_IR_F_RIGHT) >20); stop(); straightWallLeft();

			c = 0;
			straightWallLeft();
			for (int i = 0; i < 100; i++)
				if (abs(getDistance(Sensor::S_IR_F_LEFT)) < 50 || abs(getDistance(Sensor::S_IR_F_RIGHT)) < 40) c++;

			if (c < 50) {

				d = (getDistance(Sensor::S_LEFT_DOWN) + getDistance(Sensor::S_LEFT_UP)) / 2.0;
				while (!_P_followLeftWall(d < 7 ? d + 2 : d) && getDistance(Sensor::BACK) < 72); stop();
				delay(50);

				straightWallLeft();
				straightWallLeft();
				delay(50);
				volatile auto t = getDistance(Sensor::S_RIGHT_UP) > 30;
				for (int i = 0; i < 25; i++)
					t |= getDistance(Sensor::S_RIGHT_UP) > 30 || getDistance(Sensor::S_RIGHT_DOWN) > 30;
				if (t) {

					analogWrite(Motors::UP_RIGHT, 100);
					analogWrite(Motors::UP_LEFT, 100);
					while (getDistance(Sensor::S_RIGHT_DOWN) < 30); stop();


					/*
					analogWrite(Motors::BACK_RIGHT, 100);
					analogWrite(Motors::BACK_LEFT, 100);
					while (getDistance(Sensor::S_RIGHT_DOWN) > 30); stop();
					*/
					stop(); delay(10);
					analogWrite(Motors::BACK_RIGHT, 140);
					analogWrite(Motors::UP_LEFT, 140);
					while (getDistance(Sensor::BACK) > 16); stop();

					stop(); delay(10);
					auto curr = getDistance(Sensor::KADIMA);
					analogWrite(Motors::BACK_RIGHT, 100);
					analogWrite(Motors::UP_LEFT, 100);
					while (curr - getDistance(Sensor::BACK) <= 5); stop();
					//straightWallLeft();

					analogWrite(Motors::UP_RIGHT, 140);
					analogWrite(Motors::UP_LEFT, 140);
					while (getDistance(Sensor::S_LEFT_DOWN) > 28 || getDistance(Sensor::S_RIGHT_DOWN) > 50); stop();

					if (getDistance(Sensor::S_LEFT_DOWN) < 28)
						straightWallLeft();

					/*
					analogWrite(Motors::BACK_RIGHT, 100);
					analogWrite(Motors::BACK_LEFT, 100);
					while (getDistance(Sensor::S_LEFT_UP) < 30); stop();
					*/


					// blow ittttt
					if (checkUV())
					{
						stop();
						analogWrite(Motors::BACK_RIGHT, 100);
						while (getDistance(Sensor::S_IR_LEFT) < 30);
						stop();
						red_led(true);
						straightCandle();
						stop(); delay(10);
						blowAfterStraight(); stop();
						red_led(false);
						home_begin();
						while (1);
					}

					analogWrite(Motors::BACK_RIGHT, 100);
					analogWrite(Motors::BACK_LEFT, 100);
					while (getDistance(Sensor::S_LEFT_UP) < 30); stop();
					delay(10);
					analogWrite(Motors::BACK_RIGHT, 100);
					analogWrite(Motors::BACK_LEFT, 100);
					while (getDistance(Sensor::BACK) > 8 || getDistance(Sensor::S_LEFT_UP) < 30); stop();

					delay(50);

					analogWrite(Motors::BACK_RIGHT, 100);
					analogWrite(Motors::BACK_LEFT, 100);
					while (getDistance(Sensor::BACK) > 8); stop();

					analogWrite(Motors::BACK_RIGHT, 100);
					analogWrite(Motors::BACK_LEFT, 100);
					while (getDistance(Sensor::BACK) > 8); stop();

					analogWrite(Motors::UP_RIGHT, 140);
					analogWrite(Motors::BACK_LEFT, 140);
					while (getDistance(Sensor::BACK) < 70); stop();

					analogWrite(Motors::UP_RIGHT, 100);
					analogWrite(Motors::BACK_LEFT, 100);
					while (getDistance(Sensor::S_IR_LEFT) > 28); stop();
					straightWallLeft();

					if (getDistance(Sensor::S_RIGHT_UP) > 30 || getDistance(Sensor::S_RIGHT_DOWN) > 30)
					{
						analogWrite(Motors::BACK_LEFT, 100);
						analogWrite(Motors::BACK_RIGHT, 100);
						while (getDistance(Sensor::S_RIGHT_UP) > 30);
						stop();
					}
				}
			}
		}

		/*
		straightWallLeft();
		analogWrite(Motors::BACK_RIGHT, 180);
		analogWrite(Motors::BACK_LEFT, 180);
		while (getDistance(Sensor::S_RIGHT_UP) < 100);
		stop(); 
		straightWallLeft();
		delay(1000);
		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::BACK) > 3); stop();
		*/

		straightWallLeft();

		if (getDistance(Sensor::S_RIGHT_UP) < 30 && getDistance(Sensor::S_RIGHT_DOWN) < 30)
		{
			auto d = (getDistance(Sensor::S_LEFT_DOWN) + getDistance(Sensor::S_LEFT_UP)) / 2.0;
			while (!_P_reverseLeftWall(d) && getDistance(Sensor::S_RIGHT_UP) < 30); stop(); 
		}

		straightWallLeft();
		delay(50);

		auto d = (getDistance(Sensor::S_LEFT_DOWN) + getDistance(Sensor::S_LEFT_UP)) / 2.0;
		while (!_P_reverseLeftWall(d) && ((getDistance(Sensor::BACK) >= 15 || getDistance(Sensor::S_RIGHT_UP) < 30))) {  } stop();
		
		stop();
		delay(50);
		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::BACK) > 8); stop();

		straightWallLeft();
		/*
		analogWrite(Motors::UP_LEFT, 140);
		analogWrite(Motors::BACK_RIGHT, 140);
		while (abs(getDistance(Sensor::S_IR_RIGHT)) < 60);
		stop();
		analogWrite(Motors::UP_LEFT, 140);
		analogWrite(Motors::BACK_RIGHT, 140);
		while (abs(getDistance(Sensor::S_IR_F_LEFT)) < 60 && abs(getDistance(Sensor::S_IR_RIGHT)) > 25);
		stop();
		*/

		analogWrite(Motors::UP_LEFT, 140);
		analogWrite(Motors::BACK_RIGHT, 140);
		while (abs(getDistance(Sensor::S_IR_RIGHT)) < 80); stop();
		analogWrite(Motors::UP_LEFT, 100);
		analogWrite(Motors::BACK_RIGHT, 100);
		while (abs(getDistance(Sensor::S_IR_F_RIGHT)) < 50 || abs(getDistance(Sensor::S_IR_F_LEFT)) < 80); stop();
		analogWrite(Motors::UP_LEFT, 100);
		analogWrite(Motors::BACK_RIGHT, 100);
		while (abs(getDistance(Sensor::S_IR_RIGHT)) > 28);
		stop();
		straightWallRight();

		if (abs(getDistance(Sensor::S_IR_RIGHT)) > 80)
		{

			analogWrite(Motors::UP_LEFT, 140);
			analogWrite(Motors::BACK_RIGHT, 140);
			while (abs(getDistance(Sensor::S_IR_RIGHT)) < 80); stop();
			analogWrite(Motors::UP_LEFT, 100);
			analogWrite(Motors::BACK_RIGHT, 100);
			while (abs(getDistance(Sensor::S_IR_F_RIGHT)) < 50 || abs(getDistance(Sensor::S_IR_F_LEFT)) < 100); stop();
			analogWrite(Motors::UP_LEFT, 100);
			analogWrite(Motors::BACK_RIGHT, 100);
			while (abs(getDistance(Sensor::S_IR_RIGHT)) > 30);
			stop();
			straightWallRight();
		}

	}

	delay(10);
	straightWallRight();
	delay(10);
	straightWallRight();

}

void home_begin()
{
	analogWrite(Motors::BACK_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	long tt = millis();
	while (getDistance(Sensor::S_IR_BACK) > 10 && millis() < tt + 400);
	stop();

	analogWrite(Motors::BACK_LEFT, 100);
	analogWrite(Motors::UP_RIGHT, 100);
	long t = millis();
	while (getDistance(Sensor::S_IR_LEFT) > 30 || millis() < t + 70);
	stop();
	delay(1000);

	analogWrite(Motors::BACK_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::S_LEFT_UP) < 30); stop();
	delay(10);
	analogWrite(Motors::BACK_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::BACK) > 8 || getDistance(Sensor::S_LEFT_UP) < 30); stop();

	delay(50);

	analogWrite(Motors::BACK_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::BACK) > 8); stop();

	analogWrite(Motors::BACK_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::BACK) > 8); stop();
	delay(1000);

	analogWrite(Motors::UP_RIGHT, 140);
	analogWrite(Motors::BACK_LEFT, 140);
	while (getDistance(Sensor::BACK) < 70); stop();

	analogWrite(Motors::UP_RIGHT, 100);
	analogWrite(Motors::BACK_LEFT, 100);
	while (getDistance(Sensor::S_IR_LEFT) > 28); stop();
	straightWallLeft();

	if (getDistance(Sensor::S_RIGHT_UP) > 30 || getDistance(Sensor::S_RIGHT_DOWN) > 30)
	{
		analogWrite(Motors::BACK_LEFT, 100);
		analogWrite(Motors::BACK_RIGHT, 100);
		while (getDistance(Sensor::S_RIGHT_UP) > 30);
		stop();
	}

		

		/*
		straightWallLeft();
		analogWrite(Motors::BACK_RIGHT, 180);
		analogWrite(Motors::BACK_LEFT, 180);
		while (getDistance(Sensor::S_RIGHT_UP) < 100);
		stop();
		straightWallLeft();
		delay(1000);
		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::BACK) > 3); stop();
		*/

		straightWallLeft();

		if (getDistance(Sensor::S_RIGHT_UP) < 30 && getDistance(Sensor::S_RIGHT_DOWN) < 30)
		{
			auto d = (getDistance(Sensor::S_LEFT_DOWN) + getDistance(Sensor::S_LEFT_UP)) / 2.0;
			while (!_P_reverseLeftWall(d) && getDistance(Sensor::S_RIGHT_UP) < 30); stop();
		}

		straightWallLeft();
		delay(50);

		auto d = (getDistance(Sensor::S_LEFT_DOWN) + getDistance(Sensor::S_LEFT_UP)) / 2.0;
		while (!_P_reverseLeftWall(d) && ((getDistance(Sensor::BACK) >= 15 || getDistance(Sensor::S_RIGHT_UP) < 30))) {} stop();

		stop();
		delay(50);
		analogWrite(Motors::BACK_RIGHT, 100);
		analogWrite(Motors::BACK_LEFT, 100);
		while (getDistance(Sensor::BACK) > 8); stop();

		straightWallLeft();
}

bool __start()
{
	return digitalRead(38) ? true : false;
}