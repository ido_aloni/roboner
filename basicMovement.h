#pragma once

#include "IPing.h"
#include "Gyro.h"

#define BASIC_V 200
#define OPTIMAL_DIS 15
#define MIN_AL_DIS 13
#define MIN_AL_DIS_LEFT 7
#define MAX_AL_DIS 13
#define MIN_DIS_FROM_WALL 25
#define CRITIC_DIS 4
#define P 1
#define P_LEFT 10
#define P_WALL 20
#define CONSTRAIN_TOP_MAX (250)
#define CONSTRAIN_TOP_MIN (100)
#define ESCAPE_CONSTANT 1.2
#define ESCAPE_CONSTANT_LEFT 1.4

#define TURN_FIX_SPEED 120
#define TURN_SPEED 100
#define TURN_RIGHT_FIX 1.2
#define TURN_P_CONSTANT 0.7 //(45 degs)
#define EFF_DIS_TURN 120
#define DIS_FROM_FRONT_WALL_TURN 30
#define Tt_TURN 150
#define WAIT_BEFORE_TURN 100


typedef enum
{
	UP_LEFT = 8,
	UP_RIGHT = 10,
	BACK_LEFT = 9,
	BACK_RIGHT = 11
} Motors;

static int pivot = 0;

void initMovement();

void led(bool s) {
	if (s)
	{
		digitalWrite(22, HIGH);
		digitalWrite(26, HIGH);
		digitalWrite(30, HIGH);

		digitalWrite(31, HIGH);
		digitalWrite(27, HIGH);
		digitalWrite(23, HIGH);
	}
	else
	{
		digitalWrite(22, LOW);
		digitalWrite(26, LOW);
		digitalWrite(30, LOW);

		digitalWrite(31, LOW);
		digitalWrite(27, LOW);
		digitalWrite(23, LOW);
	}
}

void beep()
{
	analogWrite(A1, 150); delay(400); analogWrite(A1, 0);
}

/*
need to be iterated.
returns false it reached the wall limit
else true
*/
bool followRightWall();
bool followLeftWall();
void set_pivot(int p) { pivot = p; }
int get_pivot() { return pivot; }
bool turnLeft();
bool turnRight();
void compact_turn();
void turn_right_90();
void stop();

bool _P_followLeftWall(double optimal);
bool _P_followRightWall(double optimal);
bool _P_reverseLeftWall(double optimal);
bool _P_reverseRightWall(double optimal);
void straightWallRight();
void straightWallLeft();

void DiscoDiscoGoodGood();