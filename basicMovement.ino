#include "basicMovement.h"


void initMovement()
{
	pinMode(9, OUTPUT);
	pinMode(8, OUTPUT);
	pinMode(10, OUTPUT);
	pinMode(11, OUTPUT);
}

void stop()
{
	analogWrite(Motors::BACK_LEFT, 0);
	analogWrite(Motors::BACK_RIGHT, 0);
	analogWrite(Motors::UP_LEFT, 0);
	analogWrite(Motors::UP_RIGHT, 0);
}

void turn_right_90()
{
	stop();
	int org_value = get_gyro_deg(Z, X);
	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::BACK_RIGHT, 100);
	while (org_value > 270 ? ((int)get_gyro_deg(Z, X)>0 ? (360 - org_value + get_gyro_deg(Z, X)<87) : (get_gyro_deg(Z, X) - org_value < 87)) : ((int)get_gyro_deg(Z, X) - org_value < 87))
	{
	}
	stop();
}

bool turnLeft()
{
	double volatile dis = getDistance(Sensor::KADIMA);

	stop();
	delay(WAIT_BEFORE_TURN);

	if (dis < DIS_FROM_FRONT_WALL_TURN)
	{
		// save the last distance
		double last_dis = getDistance(Sensor::S_IR_RIGHT);

		analogWrite(Motors::UP_RIGHT, TURN_SPEED);
		analogWrite(8, 0);
		analogWrite(Motors::BACK_LEFT, TURN_SPEED);
		analogWrite(11, 0);

		// turn a bit
		while (getDistance(Sensor::KADIMA) + 3 <= last_dis);
		// turn 
		stop(); //delay(100);
		double dis_of_beg_second_turn = getDistance(Sensor::S_IR_RIGHT);
		analogWrite(Motors::UP_RIGHT, TURN_SPEED);
		analogWrite(8, 0);
		analogWrite(Motors::BACK_LEFT, TURN_SPEED);
		analogWrite(11, 0);
		while ((dis_of_beg_second_turn / (TURN_RIGHT_FIX*getDistance(Sensor::S_IR_RIGHT)) > TURN_P_CONSTANT));
		stop(); //delay(100);
		// fix
		analogWrite(10, 0);
		analogWrite(9, 0);
		analogWrite(Motors::BACK_RIGHT, TURN_FIX_SPEED);
		analogWrite(Motors::UP_LEFT, TURN_FIX_SPEED);
		//while (getDistance(Sensor::S_RIGHT_UP) / getDistance(Sensor::S_RIGHT_DOWN) > 1);
		while (getDistance(Sensor::S_RIGHT_UP) / getDistance(Sensor::S_RIGHT_DOWN) > 1);
		stop(); 
	}

	return true;
}

void compact_turn()
{
	stop();
	analogWrite(Motors::UP_LEFT, 115);
	analogWrite(Motors::BACK_RIGHT, 100);
	unsigned long startTime = millis();

	while (getDistance(Sensor::S_IR_RIGHT) > 30 && millis() - startTime < 200) {};
	stop();
	delay(150);

	analogWrite(Motors::UP_LEFT, 100);
	analogWrite(Motors::UP_RIGHT, 100);
	while (getDistance(Sensor::S_RIGHT_DOWN) > 50) {}
	stop();

}

bool turnRight()
{

	double volatile dis = getDistance(Sensor::KADIMA);

	stop();
	delay(WAIT_BEFORE_TURN);

	if (dis < DIS_FROM_FRONT_WALL_TURN)
	{
		// save the last distance
		double last_dis = getDistance(Sensor::S_IR_LEFT);

		analogWrite(Motors::BACK_RIGHT, TURN_SPEED);
		analogWrite(10, 0);
		analogWrite(Motors::UP_LEFT, TURN_SPEED);
		analogWrite(9, 0);

		// turn a bit
		while (getDistance(Sensor::KADIMA) + 3 <= last_dis);
		// turn 
		stop();
		//delay(100);
		double dis_of_beg_second_turn = getDistance(Sensor::S_IR_LEFT);
		analogWrite(Motors::BACK_RIGHT, TURN_SPEED);
		analogWrite(9, 0);
		analogWrite(Motors::UP_LEFT, TURN_SPEED);
		analogWrite(10, 0);
		while ((dis_of_beg_second_turn / (TURN_RIGHT_FIX*getDistance(Sensor::S_IR_LEFT)) > TURN_P_CONSTANT));
		stop();
		//delay(100);		
		// fix
		analogWrite(8, 0);
		analogWrite(11, 0);
		analogWrite(Motors::UP_RIGHT, TURN_FIX_SPEED);
		analogWrite(Motors::BACK_LEFT, TURN_FIX_SPEED);
		while (getDistance(Sensor::S_LEFT_UP) / getDistance(Sensor::S_LEFT_DOWN) > 1);
		stop();

	}

	return true;
}

bool followRightWall()
{
	
	double vr = 0, vl = 0;
	double d1 = 0, d2 = 0;

	d1 = getDistance(Sensor::S_RIGHT_UP);
	d2 = getDistance(Sensor::S_RIGHT_DOWN);

	const double degWithWall = getDegrees(Side::RIGHT) * (PI / 180.0);
	if (degWithWall == 0)
		return;

	// 8 left 1- right
	double x = d1 - d2;

	double disWithWall = sin(degWithWall) * (0.5 * D_SENSORS + (d1 / tan(degWithWall >= 89 ? 0xFFFFFFFF : degWithWall))); //the real distance from the wall
	
	disWithWall = (d1 + d2) / 2;
																														  /*
	//Serial.println("real dis: ");
	//Serial.println(disWithWall);
	//Serial.println("x:"); Serial.println(x);
	/*Serial.println("right: ");
	Serial.println(vr);
	Serial.println("left: ");
	Serial.println(vl);*/
	//Serial.println("up: ");
	///Serial.println(getDistance(Sensor::S_RIGHT_UP));
	///Serial.println("down: ");
	///Serial.println(getDistance(Sensor::S_RIGHT_DOWN));
	
	double x1 = pow(abs(disWithWall - OPTIMAL_DIS), 1) * 2 * (disWithWall - OPTIMAL_DIS > 0 ? 1 : -1);
	double x2 = pow(abs(d1 - d2), 1) * 3 *(d1 - d2 > 0 ? 1 : -1);

	/*
	if (abs(disWithWall - OPTIMAL_DIS) > 2) { x = x1; }
	else x = x2;
	*/

	x = x1 + x2;

	vr = constrain(BASIC_V - x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);
	vl = constrain(BASIC_V + x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);

	analogWrite(Motors::UP_LEFT, vl);
	analogWrite(Motors::UP_RIGHT, vr);

	if (getDistance(Sensor::KADIMA) < MIN_DIS_FROM_WALL)
	{
		return true;
	}
	else
	{
		return false;
	}

}

bool followLeftWall()
{
	double vr = 0, vl = 0;
	double d1 = 0, d2 = 0;

	d1 = getDistance(Sensor::S_LEFT_UP);
	d2 = getDistance(Sensor::S_LEFT_DOWN);

	// 8 left 1- right
	double x = d1 - d2;

	double disWithWall = (d1 + d2) / 2;
	/*
	//Serial.println("real dis: ");
	//Serial.println(disWithWall);
	//Serial.println("x:"); Serial.println(x);
	/*Serial.println("right: ");
	Serial.println(vr);
	Serial.println("left: ");
	Serial.println(vl);*/
	//Serial.println("up: ");
	///Serial.println(getDistance(Sensor::S_RIGHT_UP));
	///Serial.println("down: ");
	///Serial.println(getDistance(Sensor::S_RIGHT_DOWN));

	double x1 = pow(abs(disWithWall - OPTIMAL_DIS), 1) * 2 * (disWithWall - OPTIMAL_DIS > 0 ? 1 : -1);
	double x2 = pow(abs(d1 - d2), 1) * 3 * (d1 - d2 > 0 ? 1 : -1);

	/*
	if (abs(disWithWall - OPTIMAL_DIS) > 2) { x = x1; }
	else x = x2;
	*/

	x = x1 + x2;

	vr = constrain(BASIC_V + x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);
	vl = constrain(BASIC_V - x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);

	analogWrite(Motors::UP_LEFT, vl);
	analogWrite(Motors::UP_RIGHT, vr);

	if (getDistance(Sensor::KADIMA) < MIN_DIS_FROM_WALL)
	{
		return true;
	}
	else
	{
		return false;
	}

}


bool _P_m_followRightWall(double optimal, double speed)
{
	static bool sflag = true;
	double vr = 0, vl = 0;
	double d1 = 0, d2 = 0;

	d1 = getDistance(Sensor::S_RIGHT_UP);
	d2 = getDistance(Sensor::S_RIGHT_DOWN);

	const double degWithWall = getDegrees(Side::RIGHT) * (PI / 180.0);
	if (degWithWall == 0)
		return;

	// 8 left 1- right
	double x = d1 - d2;

	double disWithWall = sin(degWithWall) * (0.5 * D_SENSORS + (d1 / tan(degWithWall >= 89 ? 0xFFFFFFFF : degWithWall))); //the real distance from the wall

	disWithWall = (d1 + d2) / 2;

	//double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	//double x2 = pow(abs(d1 - d2), 1) * 3 * (d1 - d2 > 0 ? 1 : -1);
	double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	double x2 = pow(abs(d1 - d2), 1) * 5 * (d1 - d2 > 0 ? 1 : -1);

	if (abs(disWithWall - optimal) == 1)
	{
		if (sflag && abs(d1 - d2)>3)
		{
			if (abs(random()) % 1)
				straightWallRight();
			sflag = false;
		}
		x = x1 * 3;
	}
	else
	{
		sflag = true;
		x = x1 + x2;
	}

	vr = constrain(speed - x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);
	vl = constrain(speed + x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);

	analogWrite(Motors::UP_LEFT, vl);
	analogWrite(Motors::UP_RIGHT, vr);

	if (getDistance(Sensor::KADIMA) < MIN_DIS_FROM_WALL)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool _P_followRightWall(double optimal)
{
	static bool sflag = true;
	double vr = 0, vl = 0;
	double d1 = 0, d2 = 0;

	d1 = getDistance(Sensor::S_RIGHT_UP);
	d2 = getDistance(Sensor::S_RIGHT_DOWN);

	const double degWithWall = getDegrees(Side::RIGHT) * (PI / 180.0);
	if (degWithWall == 0)
		return;

	// 8 left 1- right
	double x = d1 - d2;

	double disWithWall = sin(degWithWall) * (0.5 * D_SENSORS + (d1 / tan(degWithWall >= 89 ? 0xFFFFFFFF : degWithWall))); //the real distance from the wall

	disWithWall = (d1 + d2) / 2;

	//double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	//double x2 = pow(abs(d1 - d2), 1) * 3 * (d1 - d2 > 0 ? 1 : -1);
	double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	double x2 = pow(abs(d1 - d2), 1) * 5 * (d1 - d2 > 0 ? 1 : -1);
	
	if (abs(disWithWall - optimal) ==1 )
	{
		if (sflag && abs(d1-d2)>3)
		{
			if (abs(random()) % 1)
				straightWallRight();
			sflag = false;
		}
		x = x1 * 3;
	}
	else
	{
		sflag = true;
		x = x1 + x2;
	}

	vr = constrain(BASIC_V - x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);
	vl = constrain(BASIC_V + x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);

	analogWrite(Motors::UP_LEFT, vl);
	analogWrite(Motors::UP_RIGHT, vr);

	if (getDistance(Sensor::KADIMA) < MIN_DIS_FROM_WALL)
	{
		return true;
	}
	else
	{
		return false;
	}

}

void DiscoDiscoGoodGood()
{
	analogWrite(Motors::UP_RIGHT, 200);
	analogWrite(Motors::BACK_LEFT, 200);
	int i = 100;
	while (true)
	{
		if (i == 255)
		{
			i = 200;
		}
		analogWrite(A1, i);
		i++;
	}
}

bool _P_followLeftWall(double optimal)
{
	static bool sflag = true;
	double vr = 0, vl = 0;
	double d1 = 0, d2 = 0;

	d1 = getDistance(Sensor::S_LEFT_UP);
	d2 = getDistance(Sensor::S_LEFT_DOWN);

	const double degWithWall = getDegrees(Side::LEFT) * (PI / 180.0);
	if (degWithWall == 0)
		return;

	// 8 left 1- right
	double x = d2 - d1;

	double disWithWall = sin(degWithWall) * (0.5 * D_SENSORS + (d1 / tan(degWithWall >= 89 ? 0xFFFFFFFF : degWithWall))); //the real distance from the wall

	disWithWall = (d1 + d2) / 2;

	//double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	//double x2 = pow(abs(d1 - d2), 1) * 3 * (d1 - d2 > 0 ? 1 : -1);
	double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	double x2 = pow(abs(x), 1) * 5 * (x > 0 ? 1 : -1);

	if (abs(disWithWall - optimal) <= 1)
	{
		if (sflag && abs(x)>3)
		{
			straightWallRight();
			sflag = false;
		}
		x = x1 * 3;
	}
	else
	{
		sflag = true;
		x = x1 + x2;
	}

	vl = constrain(BASIC_V - x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);
	vr = constrain(BASIC_V + x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);

	analogWrite(Motors::UP_LEFT, vl);
	analogWrite(Motors::UP_RIGHT, vr);

	if (getDistance(Sensor::KADIMA) < MIN_DIS_FROM_WALL)
	{
		return true;
	}
	else
	{
		return false;
	}

}

bool _P_m_followLeftWall(double optimal, int speed)
{
	static bool sflag = true;
	double vr = 0, vl = 0;
	double d1 = 0, d2 = 0;

	d1 = getDistance(Sensor::S_LEFT_UP);
	d2 = getDistance(Sensor::S_LEFT_DOWN);

	const double degWithWall = getDegrees(Side::LEFT) * (PI / 180.0);
	if (degWithWall == 0)
		return;

	// 8 left 1- right
	double x = d2 - d1;

	double disWithWall = sin(degWithWall) * (0.5 * D_SENSORS + (d1 / tan(degWithWall >= 89 ? 0xFFFFFFFF : degWithWall))); //the real distance from the wall

	disWithWall = (d1 + d2) / 2;

	//double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	//double x2 = pow(abs(d1 - d2), 1) * 3 * (d1 - d2 > 0 ? 1 : -1);
	double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	double x2 = pow(abs(x), 1) * 5 * (x > 0 ? 1 : -1);

	if (abs(disWithWall - optimal) <= 1)
	{
		if (sflag && abs(x)>3)
		{
			straightWallRight();
			sflag = false;
		}
		x = x1 * 3;
	}
	else
	{
		sflag = true;
		x = x1 + x2;
	}

	vl = constrain(speed - x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);
	vr = constrain(speed + x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);

	analogWrite(Motors::UP_LEFT, vl);
	analogWrite(Motors::UP_RIGHT, vr);

	if (getDistance(Sensor::KADIMA) < MIN_DIS_FROM_WALL)
	{
		return true;
	}
	else
	{
		return false;
	}

}

void straightWallRight()
{
	for (int i = 0; i < 5; i++) {
		if ((getDistance(Sensor::S_RIGHT_UP) < getDistance(Sensor::S_RIGHT_DOWN)))
		{
			stop();
			analogWrite(Motors::BACK_LEFT, 100);
			analogWrite(Motors::UP_RIGHT, 100);
			while ((getDistance(Sensor::S_RIGHT_UP) < getDistance(Sensor::S_RIGHT_DOWN)));
		}
		else
		{
			stop();
			analogWrite(Motors::UP_LEFT, 100);
			analogWrite(Motors::BACK_RIGHT, 100);
			while ((getDistance(Sensor::S_RIGHT_UP) > getDistance(Sensor::S_RIGHT_DOWN)));
		}
		stop();
	}
}

void straightWallLeft()
{
	for (int i = 0; i < 5; i++) {
		if ((getDistance(Sensor::S_LEFT_UP) < getDistance(Sensor::S_LEFT_DOWN)))
		{
			stop();
			analogWrite(Motors::UP_LEFT, 100);
			analogWrite(Motors::BACK_RIGHT, 100);
			while ((getDistance(Sensor::S_LEFT_UP) < getDistance(Sensor::S_LEFT_DOWN)));
		}
		else
		{
			stop();
			analogWrite(Motors::BACK_LEFT, 100);
			analogWrite(Motors::UP_RIGHT, 100);
			while ((getDistance(Sensor::S_LEFT_UP) > getDistance(Sensor::S_LEFT_DOWN)));
		}
		stop();
	}
}

bool _P_reverseLeftWall(double optimal)
{
	static bool sflag = true;
	double vr = 0, vl = 0;
	double d1 = 0, d2 = 0;

	d1 = getDistance(Sensor::S_LEFT_UP);
	d2 = getDistance(Sensor::S_LEFT_DOWN);

	const double degWithWall = getDegrees(Side::RIGHT) * (PI / 180.0);
	if (degWithWall == 0)
		return;

	// 8 left 1- right
	double x = d2 - d1;

	double disWithWall = sin(degWithWall) * (0.5 * D_SENSORS + (d1 / tan(degWithWall >= 89 ? 0xFFFFFFFF : degWithWall))); //the real distance from the wall

	disWithWall = (d1 + d2) / 2;

	//double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	//double x2 = pow(abs(d1 - d2), 1) * 3 * (d1 - d2 > 0 ? 1 : -1);
	double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	double x2 = pow(abs(x), 1) * 5 * (x > 0 ? 1 : -1);

	if (abs(disWithWall - optimal) <= 1)
	{
		if (sflag && abs(x)>3)
		{
			straightWallLeft();
			sflag = false;
		}
		x = x1 * 3;
	}
	else
	{
		sflag = true;
		x = x1 + x2;
	}

	vl = constrain(BASIC_V - x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);
	vr = constrain(BASIC_V + x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);

	analogWrite(Motors::BACK_LEFT, vl);
	analogWrite(Motors::BACK_RIGHT, vr);

	if (getDistance(Sensor::BACK) < MIN_DIS_FROM_WALL)
	{
		//return true;
		return false;
	}
	else
	{
		return false;
	}

}

bool _P_reverseRightWall(double optimal)
{
	static bool sflag = true;
	double vr = 0, vl = 0;
	double d1 = 0, d2 = 0;

	d1 = getDistance(Sensor::S_RIGHT_UP);
	d2 = getDistance(Sensor::S_RIGHT_DOWN);

	const double degWithWall = getDegrees(Side::RIGHT) * (PI / 180.0);
	if (degWithWall == 0)
		return;

	// 8 left 1- right
	double x = d1 - d2;

	double disWithWall = sin(degWithWall) * (0.5 * D_SENSORS + (d1 / tan(degWithWall >= 89 ? 0xFFFFFFFF : degWithWall))); //the real distance from the wall

	disWithWall = (d1 + d2) / 2;

	//double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	//double x2 = pow(abs(d1 - d2), 1) * 3 * (d1 - d2 > 0 ? 1 : -1);
	double x1 = pow(abs(disWithWall - optimal), 1) * 2 * (disWithWall - optimal > 0 ? 1 : -1);
	double x2 = pow(abs(d1 - d2), 1) * 5 * (d1 - d2 > 0 ? 1 : -1);

	if (abs(disWithWall - optimal) <= 1)
	{
		if (sflag && abs(d1 - d2)>3)
		{
			straightWallRight();
			sflag = false;
		}
		x = x1 * 3;
	}
	else
	{
		sflag = true;
		x = x1 + x2;
	}

	vr = constrain(BASIC_V - x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);
	vl = constrain(BASIC_V + x*P, CONSTRAIN_TOP_MIN, CONSTRAIN_TOP_MAX);

	analogWrite(Motors::BACK_LEFT, vl);
	analogWrite(Motors::BACK_RIGHT, vr);

	if (getDistance(Sensor::BACK) < MIN_DIS_FROM_WALL)
	{
		return true;
	}
	else
	{
		return false;
	}

}